@extends('layout.admin')
@section('content')
<div class="row">
    <div class="col-lg-12">
                <!--begin::Advance Table Widget 4-->
        <div class="card card-custom card-stretch gutter-b">
            <!--begin::Header-->
            <div class="card-header border-0 py-5">
                <h3 class="card-title align-items-start flex-column">
                    <span class="card-label font-weight-bolder text-dark">Danh sách đơn hàng</span>
                </h3>
                <div class="card-toolbar">
                    <a href="/admin/create/contract" class="btn btn-success font-weight-bolder font-size-sm mr-3">Tạo mới đơn hàng</a>
                    <a href="/admin" class="btn btn-info font-weight-bolder font-size-sm">Xem báo cáo</a>
                </div>
            </div>
            <!--end::Header-->

            <!--begin::Body-->
            <div class="card-body pt-0 pb-3">
                <div class="tab-content">
                    <!--begin::Table-->
                    <div class="table-responsive">
                        <table class="table table-head-custom table-head-bg table-borderless table-vertical-center">
                            <thead>
                                <tr class="text-left text-uppercase">
                                    <th style="min-width: 250px" class="pl-7"><span class="text-dark-75">Tên khách</span></th>
                                    <th style="min-width: 100px">Sản phẩm</th>
                                    <th style="min-width: 100px">Giá bán</th>
                                    <th style="min-width: 130px">Giá nhập</th>
                                    <th style="min-width: 130px">Doanh thu</th>
                                    <th style="min-width: 130px">Tiền hàng phải trả</th>
                                    <th style="min-width: 130px">Giá nhập</th>
                                    <th style="min-width: 80px"></th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td class="pl-0 py-8">
                                        <div class="d-flex align-items-center">
                                            <div class="symbol symbol-50 symbol-light mr-4">
                                                <span class="symbol-label">
                                                    <img src="/metronic/theme/html/demo1/dist/assets/media/svg/avatars/001-boy.svg" class="h-75 align-self-end" alt="">
                                                </span>
                                            </div>
                                            <div>
                                                <a href="#" class="text-dark-75 font-weight-bolder text-hover-primary mb-1 font-size-lg">Brad Simmons</a>
                                                <span class="text-muted font-weight-bold d-block">HTML, JS, ReactJS</span>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                            $8,000,000
                                        </span>
                                        <span class="text-muted font-weight-bold">
                                            In Proccess
                                        </span>
                                    </td>
                                    <td>
                                        <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                            $520
                                        </span>
                                        <span class="text-muted font-weight-bold">
                                            Paid
                                        </span>
                                    </td>
                                    <td>
                                        <span class="text-dark-75 font-weight-bolder d-block font-size-lg">
                                            Intertico
                                        </span>
                                        <span class="text-muted font-weight-bold">
                                            Web, UI/UX Design
                                        </span>
                                    </td>
                                    <td>
                                        <img src="/metronic/theme/html/demo1/dist/assets/media/logos/stars.png" alt="image" style="width: 5.5rem">
                                        <span class="text-muted font-weight-bold d-block font-size-sm">
                                            Best Rated
                                        </span>
                                    </td>
                                    <td class="pr-0 text-right">
                                        <a href="#" class="btn btn-light-success font-weight-bolder font-size-sm">View Offer</a>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <!--end::Table-->
                </div>
            </div>
            <!--end::Body-->
        </div>
<!--end::Advance Table Widget 4-->
	</div>
</div>
@endsection