<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller as Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class AdminController extends Controller
{
    // //
    // /**
    //  * Create a new controller instance.
    //  * @return void
    //  */
    // public function __construct()
    // {
    //     $this->middleware('auth');
    // }


    public function index()
    {
        // Test database connection
        try {
            DB::connection()->getPdo();
        } catch (\Exception $e) {
            die("Could not connect to the database.  Please check your configuration. error:" . $e);
        }
        $user = [
            'name'=> 'abc',
        ];
        return view('admin.dashboard', ['user'=> $user]);
    }

    public function create()
    {
        $user = [
            'name'=> 'abc',
        ];
        return view('admin.addcontract', ['user'=> $user]);
    }

    public function list()
    {
        $user = [
            'name'=> 'abc',
        ];
        return view('admin.contract', ['user'=> $user]);
    }
}
